"""
Configuration
"""

from pathlib import Path
import json
import logging
from typing import Optional, Tuple, Union

import PIL as Pillow


LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


# Metadata
SITENAME = "Nachhaltiges Design für eine bessere Welt"
SITEDESC = (
    "Wir planen, errichten und betreuen kleine und große Webprojekte für alle, "
    + "die mit gesundem Idealismus die Welt ein bisschen besser machen wollen."
)

SITEURL = "http://127.0.0.1:8000"

# Localization
TIMEZONE = "Europe/Berlin"
DEFAULT_LANG = "de"

# Disable feed generation
FEED_ALL_ATOM = None

# Define custom theme
THEME = "theme"

# Prevent archive pages
DIRECT_TEMPLATES = ["index"]

# Copy static files
STATIC_PATHS = [
    "favicon.ico",
    "pubkey.asc",
    "robots.txt",
]

# Load data from
with open("package.json", "r", encoding="UTF-8") as json_file:
    data = json.load(json_file)

# Email address
MAIL = data["author"]["email"]

# Repository
REPO_URL = data["repository"]["url"]

# Public GPG key
PUBKEY_URL = SITEURL + "/pubkey.asc"

# Plugin settings
WEBCOMPRESSOR_ENABLE_IMAGEOPTIM = True
WEBCOMPRESSOR_ENABLE_MODERN_FORMATS = True
WEBCOMPRESSOR_ENABLE_CSP = True
WEBCOMPRESSOR_ENABLE_MINIFY = False
WEBCOMPRESSOR_JPEG_QUALITY = 80


def _img2pxl(
    file: Union[Path, str],
    output: Union[Path, str, None] = None,
    pixels: Tuple[int, int] = (16, 16),
    dimensions: Optional[Tuple[int, int]] = None,
) -> Path:
    """
    Pixelizes single image

    :param file: pathlib.Path | str Path to input file
    :param file: pathlib.Path | str Path to output file
    :param pixels: tuple Pixel count
    :param dimensions: tuple | None Image size
    :return: pathlib. Pixelized image file
    """

    # Convert file (if needed)
    if isinstance(file, str):
        file = Path(file)

    # Load image
    with Pillow.Image.open(file) as image:
        # If not specified ..
        if dimensions is None:
            # .. use dimensipons of original image
            dimensions = (image.width, image.height)

        # Pixelize it
        pixelized = image.resize(pixels, Pillow.Image.Resampling.BILINEAR)

        # Resize & save it
        pixelized.resize(dimensions, Pillow.Image.Resampling.NEAREST).save(
            output or Path(file.parents[0], f"{file.stem}-pxl{file.suffix}")
        )

        # Close file pointer
        pixelized.close()


def img2pxl(asset: Tuple[Path, str]) -> None:
    """
    Pixelizes JPEG images below 'testimonials' directory

    :param asset: tuple Path to file & its MIME type
    :return: None
    """

    # Unpack asset file & mediatype
    file, mime_type = asset

    # If image file meets criteria ..
    if mime_type == "image/jpeg":
        # .. pixelize it
        _img2pxl(file)

        # .. report back
        LOGGER.info(f"Pixelized {file.name}")


WEBCOMPRESSOR_PRE_HOOK = img2pxl

# https://pagespeed.web.dev/report?url=https://digitalbuero.eu
WEBCOMPRESSOR_CSP_DIRECTIVES = {
    "default-src": "'none'",
    "img-src": "'self' data: w3.org/svg/2000",
    "require-trusted-types-for": "'script'",
    # Declare fallbacks for older Safari
    "script-src": "'strict-dynamic' 'unsafe-inline' http: https:",
    "style-src": "'strict-dynamic'",
    # Declare directives not governed by 'default-src'
    "base-uri": "'none'",
    "object-src": "'none'",
}

# Apply 'link' class to legal references
GESETZE_CUSTOM_ATTRIBUTES = {"class": "link"}

# Enable plugins for ..
PLUGINS = [
    # .. linking legal norms
    "pelican.plugins.gesetzify",
    # .. loading data files
    "pelican.plugins.data_files",
    # .. supercharging HTML files & web assets
    "pelican.plugins.compressor",
]
