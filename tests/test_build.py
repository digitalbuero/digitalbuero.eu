"""
This module is part of 'digitalbuero.eu',
which is released under GPL-3.0-only license.
"""

import pathlib

from pelican import Pelican, get_instance

from .config import Config


def test_development(tmp_path: pathlib.Path) -> None:
    """
    Tests development build (using 'Pelican')

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    config = Config(tmp_path, "pelicanconf.py")
    pelican: Pelican = get_instance(config)[0]

    # Assert result
    assert pelican.run() is None


def test_production(tmp_path: pathlib.Path) -> None:
    """
    Tests production build (using 'Pelican')

    :param tmp_path: pathlib.Path Virtual directory
    :return: None
    """

    # Setup
    config = Config(tmp_path, "publishconf.py")
    pelican: Pelican = get_instance(config)[0]

    # Assert result
    assert pelican.run() is None
