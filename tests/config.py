"""
This module is part of 'digitalbuero.eu',
which is released under GPL-3.0-only license.
"""

# pylint: disable=too-few-public-methods

from pathlib import Path


class Config:
    """
    Provides configuration for testing 'Pelican'
    """

    def __init__(self, tmp_path: Path, config_file: str) -> None:
        """
        Constructor

        :param tmp_path: pathlib.Path Virtual directory
        :param config_file: str Configuration filename
        :return: None
        """

        # Define root directory
        root_dir = Path(__file__).parent.parent

        # Provide default settings (= absolute minimum)
        for key, value in {
            "settings": root_dir / config_file,
            "path": root_dir / "content",
            "output": tmp_path,
            "theme": root_dir / "themes" / "v2",
            "delete_outputdir": None,
            "output_retention": None,
            "ignore_cache": None,
            "cache_path": None,
            "selected_paths": None,
            "relative_paths": None,
            "port": None,
            "bind": None,
            "verbosity": None,
            "overrides": [],
        }.items():
            setattr(self, key, value)
