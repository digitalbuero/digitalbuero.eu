const colors = require('windicss/colors')

module.exports = {
    extract: {
        include: [
            'src/css/*.html',
            'src/js/**/*.js',
            'themes/v2/templates/*.html',
            'themes/v2/templates/icons/*.svg',
            'themes/v2/templates/partials/*.html',
        ],
    },
    safelist: [
        // Icon sizes
        'w-4 h-4',
        'w-5 h-5',
        'w-6 h-6',
        'w-7 h-7',
        'w-8 h-8',
        'w-9 h-9',
        'w-10 h-10',

        // Quotes
        'rounded-full',

        // Portfolio
        'hidden',
        'absolute',
        'rounded-sm',
    ],
    darkMode: false,
    theme: {
        container: {
            center: true,
        },
        screens: {
            xs: '480px',
            sm: '640px',
            md: '768px',
            lg: '1024px',
            xl: '1280px',
        },
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            primary: colors.teal,
            secondary: colors.amber,

            white: colors.white,
            gray: colors.gray,
        },
        fontFamily: {
            sans: [
                'Fira Sans',
                'ui-sans-serif',
                'system-ui',
                '-apple-system',
                'BlinkMacSystemFont',
                'Segoe UI',
                'Roboto',
                'Helvetica Neue',
                'Arial',
                'Noto Sans',
                'sans-serif',
            ],
            mono: [
                'IBM Plex Mono',
                'Fira Mono',
                'Hack',
                'ui-monospace',
                'SFMono-Regular',
                'Menlo',
                'Monaco',
                'Consolas',
                'Liberation Mono',
                'Courier New',
                'monospace',
            ],
        },
    },
    shortcuts: {
        'heading': 'font-mono font-bold text-primary-600',
        'heading--dark': 'font-mono font-bold text-white',
        'subheading': 'font-bold text-primary-400 uppercase tracking-wide',
        'subheading--link': 'border-b-3 border-transparent hover:border-primary-400',
        'link': 'font-medium text-secondary-400 hover:text-secondary-600 transition',
        'btn': 'px-4 py-2 inline-block font-mono font-medium text-lg text-white transition rounded',
        'btn-primary': 'bg-primary-400 hover:bg-primary-500',
        'btn-primary--dark': 'bg-primary-600 hover:bg-primary-500',
        'btn-secondary': 'bg-secondary-400 hover:bg-secondary-500',
        'btn-secondary--dark': 'bg-secondary-600 hover:bg-secondary-500',
        'bg-circuits': 'bg-gray-100 bg-hero-circuit-board',
        'bg-circuits-alt': 'bg-primary-800 bg-hero-circuit-board-alt',
        'quote': 'font-mono font-bold text-4xl text-center italic',
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@windicss/plugin-heropatterns')({
            patterns: ['circuit-board', 'signal'],
            colors: {
                'default': '#e5e7eb',  // gray-200
                'alt': '#134e4a',      // teal-900
            },
            opacity: {
                default: '1.0',
                // 25: '0.25',
            },
        }),
    ],
};
