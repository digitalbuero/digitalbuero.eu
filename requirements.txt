black==22.10.0
isort==5.10.1
pelican==4.8.0
pelican-compressor==0.2.0
pelican-data-files==1.2.0
pelican-gesetze==0.1.0
Pillow==9.2.0              # pelican-compressor[images]
pillow-avif-plugin==1.2.2  # pelican-compressor[images]
pylint==2.15.3
pytest==7.1.3
pytest-cov==4.0.0
pytest-sugar==0.9.5
PyYAML==6.0                # pelican-data-files[yaml]
tdewolff-minify==2.12.4    # pelican-compressor[minify]
