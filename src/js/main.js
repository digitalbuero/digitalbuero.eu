'use strict';

/*
* Imports
*/

import gallery from './partials/gallery';
import jsDetect from './partials/jsDetect';
import lazyLoad from './partials/lazyLoad';
import scrollProgress from './partials/scrollProgress';
import typewriter from './partials/typewriter';

/*
* App Class
*/

class App {
    static start() {
        return new App();
    }

    constructor() {
        Promise
        .all([
            App.domReady(),
        ])
        .then(this.init.bind(this));
    }

    static domReady() {
        return new Promise((resolve) => {
            document.addEventListener('DOMContentLoaded', resolve);
        });
    }

    static showPage() {
        document.body.classList.add('app:is-ready');
        console.info('🚀 App:ready');
    }

    init() {
        console.info('🚀 App:init');

        // Avoid 'blank page' on JS error
        try {
            // Check whether JavaScript works
            jsDetect();

            // Add portfolio changer
            gallery();

            // Lazyload images
            lazyLoad();

            // Add scroll progress bar
            scrollProgress();

            // Add typewriter effect
            typewriter();

        } catch (err) {
            console.error(err);
        }

        App.showPage();
    }
}

App.start();
