import { clearInterval, setInterval } from 'worker-timers'


export default class Gallery {
    // Duration
    duration = 0.075

    constructor(className) {
        // Retrieve images
        this.images = document.getElementsByClassName(className)

        // Store 'z-index' of topmost image
        this.top = 1

        // Store index of current image
        this.cur = this.images.length - 1

        // Set opacity of all images to maximum
        for (let i = 0; i < this.images.length; ++i) {
            this.images[i].style.opacity = 1
        }


        // Swaps current image with next one
        this.changeImage = async () => {
            // Store index of next image
            const nextImage = (this.cur + 1) % this.images.length

            // Increase 'z-index' of current & next image
            this.images[this.cur].style.zIndex = this.top + 1
            this.images[nextImage].style.zIndex = this.top

            // Animate image transition & wait until finished
            await new Promise((resolve) => {
                // Store timer while ..
                const id = setInterval(() => {
                    // .. decreasing opacity of current image ..
                    this.images[this.cur].style.opacity -= this.duration

                    // .. until its opacity hits minimum
                    if (this.images[this.cur].style.opacity <= 0) {
                        // From there, exit timer & resolve promise
                        clearInterval(id)
                        resolve()
                    }
                }, 1 / this.duration)
            })

            // Reverse positions by ..
            // .. resetting 'z-index' of current image
            this.images[this.cur].style.zIndex = this.top

            // .. increasing 'z-index' of next image to (more than) maximum
            this.images[nextImage].style.zIndex = this.top + 1

            // Increment 'z-index' of topmost image
            this.top = this.top + 1

            // Change opacity of current image back to maximum
            this.images[this.cur].style.opacity = 1

            // Set next image as current one
            this.cur = nextImage
        }
    }
}

// For more information,
// see https://www.geeksforgeeks.org/image-transition-with-fading-effect-using-javascript
