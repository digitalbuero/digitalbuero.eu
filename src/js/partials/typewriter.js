import malarkey from 'malarkey'


export default function typewriter() {
    // Initialize
    malarkey((text) => {
        document.getElementById('js-typewriter').textContent = text
    }, {
        typeSpeed: 250,
        deleteSpeed: 100,
        pauseDuration: 1500,
        repeat: true,
    })
        .type("Webdesign")
        .pause()
        .delete(6)
        .type("entwicklung")
        .pause()
        .delete(11)
        .type("sites")
        .pause()
        .delete(5)
        .type("apps")
        .pause()
        .delete()
        .type("Open Source")
        .pause()
        .delete()
}
