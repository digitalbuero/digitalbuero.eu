import Loadeer from 'loadeer'


export default function lazyLoad() {
    const loadeer = new Loadeer()
    loadeer.observe()
}
