export default function scrollProgress() {
    const progress = () => {
        const scrollProgress = document.getElementById('js-scroll-progress');
        const height = document.documentElement.scrollHeight - window.innerHeight;

        const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
        scrollProgress.style.width = `${(scrollTop / height) * 100}%`;
    }

    progress()

    window.addEventListener('scroll', () => {
        progress();
    });

    window.addEventListener("resize", () => {
        progress();
    });
}
