import { TaskTimer } from 'tasktimer'

import Gallery from '../classes/Gallery'


export default function gallery() {
    // Retrieve 'index' indicator
    const identifier = document.getElementById("index")

    // If page not eligible ..
    if (typeof(identifier) == 'undefined' || identifier == null) {
        return
    }

    // Set interval to five seconds
    const interval = 5000
    const tickInterval = 100

    // Initiate timer
    const timer = new TaskTimer(interval / tickInterval);

    // Get galleries
    const desktop = new Gallery("js-desktop")
    const mobile = new Gallery("js-mobile")
    const details = new Gallery("js-details")

    // Add task for each of them
    timer.add([
        {
            id: 'desktop',
            tickInterval: tickInterval,
            totalRuns: 0,
            callback() {desktop.changeImage()}
        },
        {
            id: 'mobile',
            tickInterval: tickInterval,
            totalRuns: 0,
            callback() {mobile.changeImage()}
        },
        {
            id: 'details',
            tickInterval: tickInterval,
            totalRuns: 0,
            callback() {details.changeImage()}
        },
    ]);

    // Get progress bar
    const scrollProgress = document.getElementById('js-progress-bar');

    // Hook into cycles
    timer.on('tick', () => {
        // Determine current progress
        const progress = timer.tickCount - timer.taskRunCount / 3 * 100

        // If freshly created ..
        if (progress == 0) {
            // .. start from scratch
            scrollProgress.style.width = "0%";
        }

        // Increment width of progress bar
        scrollProgress.style.width = `${progress}%`
    })

    // Get controls
    const controls = document.getElementById("js-controls")
    const playButton = controls.querySelector(".play")
    const pauseButton = controls.querySelector(".pause")

    // When clicked, instruct ..
    // (1) .. 'play' button to ..
    playButton.addEventListener("click", () => {
        // .. adjust visibility
        pauseButton.classList.remove("hidden")
        playButton.classList.add("hidden")

        // .. resume timer
        timer.resume()
    })

    // (2) .. 'pause' button to ..
    pauseButton.addEventListener("click", () => {
        // .. adjust visibility
        playButton.classList.remove("hidden")
        pauseButton.classList.add("hidden")

        // .. pause timer
        timer.pause()
    })

    // Retrieve 'overlay' element
    const overlay = document.getElementById("js-overlay")

    // If cursor hovers it ..
    overlay.addEventListener("mouseover", () => {
        // .. pause timer
        timer.pause()
    })

    // If cursor leaves it ..
    overlay.addEventListener("mouseout", () => {
        // .. and 'play' button hidden (= timer paused) ..
        if (playButton.classList.contains("hidden")) {
            // .. resume timer
            timer.resume()
        }
    })

    // Start timer
    timer.start()
}
