"""
Configuration (production only)
"""

import os
import sys

# pylint: disable=unused-wildcard-import, wildcard-import, wrong-import-position
sys.path.append(os.curdir)
from pelicanconf import *


# Metadata
SITEURL = "https://digitalbuero.eu"

# Build fresh content
DELETE_OUTPUT_DIRECTORY = True

# Plugin settings
WEBCOMPRESSOR_ENABLE_MINIFY = True
