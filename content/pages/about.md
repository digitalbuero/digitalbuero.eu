title: Transparenz
template: about
url: transparenz
save_as: transparenz/index.html


### Bildquellen

Die verwendeten Bilder von <a class="link" href="https://commons.wikimedia.org/wiki/File:LinusTorvalds.jpg" target="_blank">Linus Torvalds</a> und <a class="link" href="https://commons.wikimedia.org/wiki/File:Sir_Tim_Berners-Lee.jpg" target="_blank">Tim Berners-Lee</a> wurden der Wikimedia Commons entnommen, zum Einsatz auf unserer Webseite verändert und stehen - wie alle weiteren Inhalte - unter der <a class="link" href="https://www.gnu.org/licenses/gpl-3.0.de.html" target="_blank">GNU Public License v3</a>.


### Kolophon

Unsere Webseite wurde mit <a class="link" href="https://getpelican.com" target="_blank">Pelican</a> umgesetzt und liegt auf einem Webspace von <a class="link" href="https://uberspace.de" target="_blank">Uberspace</a>, dem wohl außergewöhnlichsten Hostinganbieter Deutschlands.

Das CSS-Framework <a class="link" href="https://windicss.org" target="_blank">WindiCSS</a> sorgt für frischen Wind, die Symbolbilder der <a class="link" href="https://heroicons.com" target="_blank">Heroicons</a> und ein Hintergrundmuster der <a class="link" href="https://heropatterns.com" target="_blank">Heropatterns</a> setzen visuelle Akzente - auf großformatige Hintergrundbilder und das Einbinden von Schriftarten haben wir hingegen <a class="link" href="https://cleaner-web.com" target="_blank">bewusst verzichtet</a>. So werden nur Schriftarten verwendet, die auf dem jeweiligen Gerät vorhanden sind - als "Serviervorschlag" empfehlen wir eine Kombination aus <a class="link" href="https://www.ibm.com/plex" target="_blank">IBM Plex Mono</a> und <a class="link" href="https://mozilla.github.io/Fira" target="_blank">Fira Sans</a>.

Für ein wenig Abwechslung sorgen schlanke JavaScript-Bibliotheken, mit denen <a class="link" href="https://github.com/chrisguttandin/worker-timers" target="_blank">ständig</a> <a class="link" href="https://onury.io/tasktimer" target="_blank">durchwechselnde Bilder</a> auf der Hauptseite, der <a class="link" href="https://www.typeitjs.com" target="_blank">Schreibmaschineneffekt</a> im oberen Bereich sowie das <a class="link" href="https://github.com/johannschopplich/loadeer" target="_blank">nachträgliche Laden von Medien</a> erst dann, wenn sie sich dem sichtbaren Bereich nähern, realisiert wurden.


### Danksagung

Wir danken allen, deren unermüdlicher Einsatz für <a class="link" href="https://de.wikipedia.org/wiki/Freie_Software" target="_blank">offene und freie Software</a> in unzähligen Projekten seinen Niederschlag findet - you guys are awesome!
